from django.shortcuts import render, HttpResponse, HttpResponseRedirect, redirect
from .models import *
from datetime import *

# Create your views here.

#TO DO : Benerin returnnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
def index(request):
    data = {}
    if request.session.get('user') is not None:
        if request.session['user']['role'] == 'konsumen':
            data['transaksi_pembelians'] = get_transaksi_pembelian_konsumen(get_id_konsumen(
                request.session['user']['email'])[0])
        else:
            data['transaksi_pembelians'] = fetch_all_transaksi_pembelian()
        return render(request,'transaksi_pembelian/index.html', data)
    else:
        return redirect('authentication:index')

def create(request):
    data = {}
    if request.session.get('user'):
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == "GET":
                data['id_konsumens'] = get_all_id_konsumen_from_konsumen()
                return render(request,'transaksi_pembelian/create.html', data)
            else:
                current_biggest_id_tp = get_biggest_id_transaksi_pembelian()
                current_biggest_id_tp_number = ''.join(filter(str.isdigit, current_biggest_id_tp))
                current_biggest_id_tp_alphabet = current_biggest_id_tp.strip(current_biggest_id_tp_number)
                new_id_transaksi_pembelian = int(current_biggest_id_tp_number) + 1

                data['id_transaksi_pembelian'] = current_biggest_id_tp_alphabet + str(new_id_transaksi_pembelian)
                data['id_konsumen'] = request.POST['idKonsumen']
                insert_transaksi_pembelian(data)
                return redirect('transaksi_pembelian:index')
        else:
            return redirect('transaksi_pembelian:index')
    else:
        return redirect('authentication:index')

def edit(request, id_transaksi_pembelian):
    data = {}
    if request.session.get('user'):
        if (request.session['user']['role'] == 'admin_apotek'):
            if request.method == "GET":
                if any(transaksi_pembelian := get_transaksi_pembelian(id_transaksi_pembelian)):
                    data['transaksi_pembelian'] = transaksi_pembelian[0]
                    data['id_konsumens'] = get_all_id_konsumen_from_konsumen()
                    return render(request,'transaksi_pembelian/edit.html', data)
                else:
                    return redirect('transaksi_pembelian:index')
            else:
                data['id_transaksi_pembelian'] = id_transaksi_pembelian
                data['waktu_pembelian'] = request.POST['waktuPembelian']
                data['total_pembayaran'] = request.POST['totalPembayaran']
                data['id_konsumen'] = request.POST['idKonsumen']
                print(data)
                update_transaksi_pembelian(data)
                return redirect('transaksi_pembelian:index')
        else:
            return redirect('transaksi_pembelian:index')
    else:
        return redirect('authentication:index')

def delete(request, id_transaksi_pembelian):
    if request.session.get('user'):
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == 'POST':
                delete_transaksi_pembelian(id_transaksi_pembelian)
        return redirect('transaksi_pembelian:index')
    else:
        return redirect('authentication:index')