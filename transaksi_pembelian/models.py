from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_transaksi_pembelian():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM transaksi_pembelian")
    return dictfetchall(cursor)

def get_all_id_transaksi_pembelian():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_transaksi_pembelian FROM TRANSAKSI_PEMBELIAN")
        return [row[0] for row in cursor.fetchall()]

def get_all_id_konsumen_from_konsumen():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_konsumen FROM KONSUMEN")
        return [row[0] for row in cursor.fetchall()]

def get_id_konsumen(email):
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_konsumen FROM KONSUMEN WHERE email = %s",
        [
            email
        ])
        return [row[0] for row in cursor.fetchall()]

def get_biggest_id_transaksi_pembelian():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM TRANSAKSI_PEMBELIAN ORDER BY id_transaksi_pembelian DESC LIMIT 1")
        if((tp:=cursor.fetchone()) is not None):
            return tp[0]
        else:
            return '0'

def get_transaksi_pembelian(id_transaksi_pembelian):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian = %s",
        [
            id_transaksi_pembelian
        ])
        return dictfetchall(cursor)

def get_transaksi_pembelian_konsumen(id_konsumen):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM TRANSAKSI_PEMBELIAN WHERE id_konsumen = %s",
        [
            id_konsumen
        ])
        return dictfetchall(cursor)

def insert_transaksi_pembelian(data):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO TRANSAKSI_PEMBELIAN VALUES(%s, NOW(), 0, %s)", 
        [
            data['id_transaksi_pembelian'], data['id_konsumen']
        ])

def update_transaksi_pembelian(data):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE TRANSAKSI_PEMBELIAN SET " + 
        "waktu_pembelian = NOW(), total_pembayaran = %s, id_konsumen = %s " +
        "WHERE id_transaksi_pembelian = %s", 
        [
            data['total_pembayaran'], data['id_konsumen']
            ,data['id_transaksi_pembelian']
        ])
        return

def delete_transaksi_pembelian(id_transaksi_pembelian):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM TRANSAKSI_PEMBELIAN WHERE id_transaksi_pembelian = %s",
        [
            id_transaksi_pembelian
        ])
        return