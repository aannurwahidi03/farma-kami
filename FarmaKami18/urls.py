"""FarmaKami18 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('authentication.urls', 'authentication'), namespace='authentication')),
    path('apotek/', include(('apotek.urls', 'apotek'), namespace='apotek')),
    path('alat_medis/', include(('alat_medis.urls', 'alat_medis'), namespace='alat_medis')),
    path('list_produk_dibeli/', include(('list_produk_dibeli.urls', 'list_produk_dibeli'), namespace='list_produk_dibeli')),
    path('pengantaran_farmasi/', include(('pengantaran_farmasi.urls', 'pengantaran_farmasi'), namespace='pengantaran_farmasi')),
    path('produk_apotek/', include(('produk_apotek.urls', 'produk_apotek'), namespace='produk_apotek')),
    path('transaksi_resep/', include(('transaksi_resep.urls', 'transaksi_resep'), namespace='transaksi_resep')),
    path('transaksi_pembelian/', include(('transaksi_pembelian.urls', 'transaksi_pembelian'), namespace='transaksi_pembelian')),
]
