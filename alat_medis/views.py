from django.shortcuts import render, HttpResponse, redirect
from .models import *

# Create your views here.

def index(request):
    data = {}
    if request.session.get('user') is not None:
        data['alat_medis'] = fetch_all_alat_medis()
        return render(request,'alat_medis/index.html', data)
    else:
        return redirect('authentication:index')

#TO DO : cek sesuaikan dengan pdf
def create(request):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek') or (request.session['user']['role'] == 'cs'):
            if request.method == 'GET':
                return render(request,'alat_medis/create.html')
            else:
                current_biggest_alat_medis_id = get_biggest_alat_medis_id()
                current_biggest_alat_medis_id_number = ''.join(filter(str.isdigit, current_biggest_alat_medis_id))
                current_biggest_alat_medis_id_alphabet = current_biggest_alat_medis_id.strip(current_biggest_alat_medis_id_number)
                new_alat_medis_id_number = int(current_biggest_alat_medis_id_number) + 1

                data['idAlatMedis'] = current_biggest_alat_medis_id_alphabet + str(new_alat_medis_id_number)
                data['namaAlatMedis'] = request.POST['namaAlatMedis']
                data['deskripsiAlat'] = request.POST['deskripsiAlat']
                data['jenisPenggunaan'] = request.POST['jenisPenggunaan']

                current_biggest_produk_id = get_biggest_produk_id()
                current_biggest_produk_id_number = ''.join(filter(str.isdigit, current_biggest_produk_id))
                current_biggest_produk_id_alphabet = current_biggest_produk_id.strip(current_biggest_produk_id_number)
                new_produk_id_number = int(current_biggest_produk_id_number) + 1
                new_produk_id = current_biggest_produk_id_alphabet + str(new_produk_id_number)

                insert_produk(new_produk_id)
                data['idProduk'] = new_produk_id

                insert_alat_medis(data)
                return redirect('alat_medis:index')
        else:
            return redirect('alat_medis:index')
    else:
        return redirect('authentication:index')

def edit(request, id_alat_medis):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek') or (request.session['user']['role'] == 'cs'):
            if request.method == 'GET':
                if any(alat_medis:= get_alat_medis(id_alat_medis)):
                    data = alat_medis[0]
                    return render(request,'alat_medis/edit.html', data)
                else:
                    return redirect('alat_medis:index')
            else:
                data['idAlatMedis'] = id_alat_medis
                data['namaAlatMedis'] = request.POST['namaAlatMedis']
                data['deskripsiAlat'] = request.POST['deskripsiAlat']
                data['jenisPenggunaan'] = request.POST['jenisPenggunaan']

                update_alat_medis(data)
                return redirect('alat_medis:index')
        else:
            return redirect('alat_medis:index')
    else:
        return redirect('authentication:index')

def delete(request, id_alat_medis):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek') or (request.session['user']['role'] == 'cs'):
            if request.method == 'POST':
                alat_medis = get_alat_medis(id_alat_medis)[0]
                delete_alat_medis(alat_medis['id_alat_medis'])
                delete_produk(alat_medis['id_produk'])
        return redirect('alat_medis:index')
    else:
        return redirect('authentication:index')