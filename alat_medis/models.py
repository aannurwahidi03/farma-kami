from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_alat_medis():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM alat_medis")
    return dictfetchall(cursor)

def get_biggest_produk_id():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM PRODUK ORDER BY id_produk DESC LIMIT 1")
    produk = cursor.fetchone()
    return produk[0]

def get_biggest_alat_medis_id():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM ALAT_MEDIS ORDER BY id_alat_medis DESC LIMIT 1")
    alat_medis = cursor.fetchone()
    return alat_medis[0]

def get_alat_medis(id_alat_medis):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM ALAT_MEDIS WHERE id_alat_medis = %s", [id_alat_medis])
    return dictfetchall(cursor)

def insert_produk(id_produk):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO PRODUK VALUES (%s)", [id_produk])
    return

def insert_alat_medis(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO ALAT_MEDIS VALUES (%s, %s, %s, %s, %s)", 
    [
        data['idAlatMedis'], data['namaAlatMedis'], data['deskripsiAlat'], data['jenisPenggunaan'], data['idProduk']
    ])
    return

def update_alat_medis(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE ALAT_MEDIS SET "+
    "nama_alat_medis = %s, deskripsi_alat = %s, jenis_penggunaan = %s "+
    "WHERE id_alat_medis = %s",
    [
        data['namaAlatMedis'], data['deskripsiAlat'], data['jenisPenggunaan'], data['idAlatMedis']
    ])

def delete_produk(id_produk):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM PRODUK WHERE id_produk= %s", [id_produk])

def delete_alat_medis(id_alat_medis):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM ALAT_MEDIS WHERE id_alat_medis = %s", [id_alat_medis])