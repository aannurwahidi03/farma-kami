from django.urls import path, include
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create, name='create'),
    path('<str:id_produk>/<str:id_apotek>/edit', views.edit, name='edit'),
    path('<str:id_produk>/<str:id_apotek>/delete', views.delete, name='delete')
]
