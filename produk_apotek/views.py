from django.shortcuts import render, HttpResponse, redirect
from .models import *

def index(request):
    data = {}
    if request.session.get('user') is not None:
        data['produk_apoteks'] = fetch_all_produk_apotek()
        return render(request,'produk_apotek/index.html', data)
    else:
        return redirect('authentication:index')

def create(request):
    data = {}
    if request.session.get('user') is not None:
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == "GET":
                data['id_apoteks'] = get_all_id_apotek_from_apotek()
                data['id_produks'] = get_all_id_produk_from_produk()
                return render(request,'produk_apotek/create.html',data)
            else:
                data['id_apoteks'] = request.POST['idApotek']
                data['id_produks'] = request.POST['idProduk']
                data['harga_jual'] = request.POST['hargaJual']
                data['satuan_penjualan'] = request.POST['satuanPenjualan']
                data['stok'] = request.POST['stok']
                if (any(produk_apotek:=get_produk_apotek(data['id_produks'], data['id_apoteks']))):
                    insert_produk_apotek(data)
                return redirect('produk_apotek:index')
        else:
            return redirect('produk_apotek:index')
    else:
        return redirect('authentication:index')

def edit(request, id_produk, id_apotek):
    data = {}
    if request.session.get('user'):
        if (request.session['user']['role'] == 'admin_apotek'):
            if request.method == "GET":
                if (any(produk_apotek:=get_produk_apotek(id_produk, id_apotek))):
                    data['produk_apotek'] = produk_apotek[0]
                    data['id_apoteks'] = get_all_id_apotek_from_apotek()
                    data['id_produks'] = get_all_id_produk_from_produk()
                    return render(request,'produk_apotek/edit.html', data)
                else:
                    return redirect('produk_apotek:index')
            else:
                data['harga_jual'] = request.POST['hargaJual']
                data['satuan_penjualan'] = request.POST['satuanPenjualan']
                data['stok'] = request.POST['stok']
                update_produk_apotek(data,id_produk,id_apotek)
                return redirect('produk_apotek:index')
        else:
            return redirect('produk_apotek:index')
    else: 
        return redirect('authentication:index')

def delete(request, id_produk, id_apotek):
    if request.session.get('user'):
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == 'POST':
                delete_produk_apotek(id_produk, id_apotek)
        return redirect('produk_apotek:index')
    else:
        return redirect('authentication:index')