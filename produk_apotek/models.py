from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_produk_apotek():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM produk_apotek")
    return dictfetchall(cursor)

def get_all_id_produk_from_produk():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_produk FROM PRODUK")
        return [row[0] for row in cursor.fetchall()]

def get_all_id_apotek_from_apotek():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_apotek FROM APOTEK")
        return [row[0] for row in cursor.fetchall()]

def get_produk_apotek(id_produk, id_apotek):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PRODUK_APOTEK WHERE id_produk = %s AND id_apotek = %s",
        [
            id_produk, id_apotek
        ])
        return dictfetchall(cursor)

def insert_produk_apotek(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO PRODUK_APOTEK VALUES (%s, %s, %s, %s, %s)", 
    [
        data['harga_jual'],  data['stok'], data['satuan_penjualan'],data['id_produks'], data['id_apoteks']
    ])
    return

def update_produk_apotek(data, id_produk, id_apotek):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE PRODUK_APOTEK SET " + 
        "harga_jual = %s, stok = %s, satuan_penjualan = %s " +
        "WHERE id_produk = %s AND id_apotek = %s", 
        [
            data['harga_jual'], data['stok'], data['satuan_penjualan'],
            id_produk, id_apotek
        ])
        return

def delete_produk_apotek(id_produk, id_apotek):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM PRODUK_APOTEK WHERE id_produk = %s AND id_apotek = %s",
        [
            id_produk, id_apotek
        ])
        return