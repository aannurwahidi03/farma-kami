from django.shortcuts import render, HttpResponse, redirect
from .models import *
from transaksi_pembelian.models import fetch_all_transaksi_pembelian
from authentication.models import fetch_all_apoteker

# Create your views here.

def index(request):
    data = {}
    if request.session.get('user') is not None:
        data['transaksi_resep'] = fetch_all_transaksi_resep()
        return render(request,'transaksi_resep/index.html', data)
    else:
        return redirect('authentication:index')

#TO DO : cek sesuaikan dengan pdf
def create(request):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek') or (request.session['user']['role'] == 'cs'):
            if request.method == 'GET':
                    data['transaksi_pembelian'] = fetch_all_transaksi_pembelian()
                    return render(request,'transaksi_resep/create.html', data)
            else:
                current_biggest_no_resep = get_biggest_no_resep()
                current_biggest_no_resep_number = ''.join(filter(str.isdigit, current_biggest_no_resep))
                current_biggest_no_resep_alphabet = current_biggest_no_resep.strip(current_biggest_no_resep_number)
                new_trans_resep_id_number = int(current_biggest_no_resep_number) + 1

                data['nomorResep'] = current_biggest_no_resep_alphabet + str(new_trans_resep_id_number)
                data['namaDokter'] = request.POST['dokter']
                data['namaPasien'] = request.POST['pasien']
                data['isiResep'] = request.POST['isiResep']
                data['unggahan'] = request.POST['unggahan']
                data['tanggalResep'] = request.POST['tanggalResep']
                data['statusResep'] = 'Waiting Approval'
                data['telepon'] = request.POST['telepon']
                data['idTransaksi'] = request.POST['idTransaksi']

                insert_transaksi_resep(data)
                return redirect('transaksi_resep:index')
        else:
            return redirect('transaksi_resep:index')

    else:
        return redirect('authentication:index')

def edit(request, no_resep):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek') or (request.session['user']['role'] == 'cs'):
            if request.method == 'GET':
                if any(get_specific_transaksi_resep(no_resep)):
                    data['transaksi_pembelian'] = fetch_all_transaksi_pembelian()
                    data['apotekers'] = fetch_all_apoteker()
                    data['transaksi_resep_item'] = get_specific_transaksi_resep(no_resep)[0]
                    return render(request,'transaksi_resep/edit.html', data)
                else:
                    return redirect('transaksi_resep:index')
            else:
                data['nomorResep'] = no_resep
                data['namaDokter'] = request.POST['dokter']
                data['namaPasien'] = request.POST['pasien']
                data['isiResep'] = request.POST['isiResep']
                data['unggahan'] = request.POST['unggahan']
                data['tanggalResep'] = request.POST['tanggalResep']
                data['statusResep'] = request.POST['statusResep']
                data['telepon'] = request.POST['telepon']
                data['idTransaksi'] = request.POST['idTransaksi']
                data['emailApoteker'] = request.POST['emailApoteker']

                update_transaksi_resep(data)
                return redirect('transaksi_resep:index')
        else:
            return redirect('transaksi_resep:index')
    else:
        return redirect('authentication:index')

def delete(request, no_resep):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek') or (request.session['user']['role'] == 'cs'):
            if request.method == 'POST':
                delete_transaksi_resep(no_resep)
        return redirect('transaksi_resep:index')

    else:
        return redirect('authentication:index')