from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_transaksi_resep():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP")
    return dictfetchall(cursor)

def get_biggest_no_resep():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP ORDER BY no_resep DESC LIMIT 1")
    transaksi_resep = cursor.fetchone()
    return transaksi_resep[0]

def get_specific_transaksi_resep(no_resep):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM TRANSAKSI_DENGAN_RESEP WHERE no_resep = %s", [no_resep])
    return dictfetchall(cursor)

def update_transaksi_resep(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE TRANSAKSI_DENGAN_RESEP SET " + 
    "nama_dokter = %s, nama_pasien = %s, isi_resep = %s, unggahan_resep = %s, tanggal_resep = %s, " + 
    "status_validasi = %s, no_telepon = %s, id_transaksi_pembelian = %s, email_apoteker = %s WHERE no_resep = %s",
    [
        data['namaDokter'], data['namaPasien'], data['isiResep'], data['unggahan'], 
        data['tanggalResep'], data['statusResep'], data['telepon'], data['idTransaksi'], data['emailApoteker'],
        data['nomorResep']
    ])
    return

def insert_transaksi_resep(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO TRANSAKSI_DENGAN_RESEP VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, NULL)", 
    [
        data['nomorResep'], data['namaDokter'], data['namaPasien'], data['isiResep'], data['unggahan'],
        data['tanggalResep'], data['statusResep'], data['telepon'], data['idTransaksi']
    ])
    return

def delete_transaksi_resep(no_resep):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM TRANSAKSI_DENGAN_RESEP WHERE no_resep = %s", [no_resep])
    return