from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create, name='create'),
    path('<str:no_resep>/edit/', views.edit, name='edit'),
    path('<str:no_resep>/delete/', views.delete, name='delete')
]
