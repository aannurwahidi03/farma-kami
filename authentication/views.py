from django.shortcuts import render, HttpResponse, redirect
from django.http import QueryDict
from .models import *

# Create your views here.

#TO DO : Benerin returnnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
def index(request):
    if(request.session.get('user') is None):
        return render(request, 'authentication/index.html')
    else:
        return redirect('authentication:profile')

#TO DO : check sesaikan dengan pdf
def register(request):
    if(request.session.get('user') is None):
        return render(request,'authentication/register.html')
    else:
        return redirect('authentication:profile')

def register_konsumen(request):
    data = {}
    if (request.method == 'GET'):
        if (request.session.get('user') is None):
            return render(request,'authentication/register_konsumen.html', data)
        else:
            return redirect('authentication:profile')
    else:
        current_biggest_id_konsumen = get_biggest_id_on_specific_table('konsumen')
        current_biggest_id_konsumen_number = ''.join(filter(str.isdigit, current_biggest_id_konsumen))
        current_biggest_id_konsumen_alphabet = current_biggest_id_konsumen.strip(current_biggest_id_konsumen_number)
        new_konsumen_id_number = int(current_biggest_id_konsumen_number) + 1

        data['email'] = request.POST['email']
        data['password'] = request.POST['password']
        data['telepon'] = request.POST['telepon']
        data['nama'] = request.POST['nama']
        data['sex'] = request.POST['sex']
        data['tanggalLahir'] = request.POST['tanggalLahir']
        data['alamats'] = list(zip(request.POST.getlist('statusAlamat', []), request.POST.getlist('namaAlamat', [])))
        data['id_konsumen'] = current_biggest_id_konsumen_alphabet + str(new_konsumen_id_number)

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'authentication/register_konsumen.html', data)
        else:
            insert_pengguna_with_role('konsumen', data)
            pengguna = get_pengguna_from_specific_table('PENGGUNA', data['email'])[0]
            pengguna['role'] = 'konsumen'
            request.session['user'] = pengguna
            return redirect('apotek:index')

def register_cs(request):
    data = {}
    if (request.method == 'GET'):
        if (request.session.get('user') is None):
            return render(request,'authentication/register_cs.html', data)
        else:
            return redirect('authentication:profile')
    else:
        data['email'] = request.POST['email']
        data['password'] = request.POST['password']
        data['telepon'] = request.POST['telepon']
        data['nama'] = request.POST['nama']
        data['no_ktp'] = request.POST['ktp']
        data['no_sia'] = request.POST['sia']

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'authentication/register_cs.html', data)
        else:
            insert_pengguna_with_role('cs', data)
            pengguna = get_pengguna_from_specific_table('PENGGUNA', data['email'])[0]
            pengguna['role'] = 'cs'
            request.session['user'] = pengguna
            return redirect('apotek:index')

def register_kurir(request):
    data = {}
    if (request.method == 'GET'):
        if (request.session.get('user') is None):
            return render(request,'authentication/register_kurir.html', data)
        else:
            return redirect('authentication:profile')
    else:
        current_biggest_id_kurir = get_biggest_id_on_specific_table('kurir')
        current_biggest_id_kurir_number = ''.join(filter(str.isdigit, current_biggest_id_kurir))
        current_biggest_id_kurir_alphabet = current_biggest_id_kurir.strip(current_biggest_id_kurir_number)
        new_kurir_id_number = int(current_biggest_id_kurir_number) + 1

        data['email'] = request.POST['email']
        data['password'] = request.POST['password']
        data['telepon'] = request.POST['telepon']
        data['nama'] = request.POST['nama']
        data['perusahaan'] = request.POST['perusahaan']
        data['id_kurir'] = current_biggest_id_kurir_alphabet + str(new_kurir_id_number)

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'authentication/register_kurir.html', data)
        else:
            insert_pengguna_with_role('kurir', data)
            pengguna = get_pengguna_from_specific_table('PENGGUNA', data['email'])[0]
            pengguna['role'] = 'kurir'
            request.session['user'] = pengguna
            return redirect('apotek:index')

def register_admin(request):
    data = {}
    if (request.method == 'GET'):
        if (request.session.get('user') is None):
            return render(request,'authentication/register_admin_apotek.html', data)
        else:
            return redirect('authentication:profile')
    else:
        data['email'] = request.POST['email']
        data['password'] = request.POST['password']
        data['telepon'] = request.POST['telepon']
        data['nama'] = request.POST['nama']

        if(any(get_pengguna_from_specific_table('PENGGUNA', data['email']))):
            return render(request, 'authentication/register_admin_apotek.html', data)
        else:
            insert_pengguna_with_role('admin_apotek', data)
            pengguna = get_pengguna_from_specific_table('PENGGUNA', data['email'])[0]
            pengguna['role'] = 'admin_apotek'
            request.session['user'] = pengguna
            return redirect('apotek:index')

def profile(request):
    data = {}
    if(request.method == 'GET') & (request.session.get('user') is not None):
        pengguna = get_pengguna_from_specific_table('PENGGUNA', request.session['user']['email'])[0]
        return render(request, 'authentication/profile.html', pengguna)
    else:
        return redirect('authentication:index')

def login(request):
    data = {}
    if(request.method == 'GET'):
        if (request.session.get('user') is None):
            return render(request, 'authentication/login.html', data)
        else:
            return redirect('authentication:profile')
    else:
        data['email'] = request.POST['email']
        data['password'] = request.POST['password']

        if any(get_pengguna_from_specific_table('PENGGUNA', data['email'])):
            pengguna = get_pengguna_from_specific_table('PENGGUNA', data['email'])[0]
            if data['password'] == pengguna['password']:

                if any(get_pengguna_from_specific_table('konsumen', data['email'])):
                    pengguna['role'] = 'konsumen'
                elif any(get_pengguna_from_specific_table('cs', data['email'])):
                    pengguna['role'] = 'cs'
                elif any(get_pengguna_from_specific_table('kurir', data['email'])):
                    pengguna['role'] = 'kurir'
                elif any(get_pengguna_from_specific_table('admin_apotek', data['email'])):
                    pengguna['role'] = 'admin_apotek'
                else:
                    pengguna['role'] = 'not_set'
                request.session['user'] = pengguna
                return redirect('apotek:index')

            else:
                return render(request, 'authentication/login.html', data)
        else:
            return render(request, 'authentication/login.html', data)

def logout(request):
    if(request.method == 'POST'):
        request.session.flush()
        return redirect('authentication:index')
    else:
        return HttpResponse('Only POST request available for LOGOUT')