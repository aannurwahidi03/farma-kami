from django.urls import path, include
from . import views

#TO DO : Tambahin URLSnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('register/konsumen/', views.register_konsumen, name='register_konsumen'),
    path('register/cs/', views.register_cs, name='register_cs'),
    path('register/admin_apotek/', views.register_admin, name='register_admin'),
    path('register/kurir', views.register_kurir, name='register_kurir'),
    path('profile/', views.profile, name='profile'),
    path('logout/', views.logout, name='logout')
]
