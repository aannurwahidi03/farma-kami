from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_apoteker():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM apoteker")
    return dictfetchall(cursor)

def fetch_all_pengguna():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM pengguna")
    return dictfetchall(cursor)

def get_pengguna_from_specific_table(table_name ,email):
    cursor = connection.cursor()
    if str.lower(table_name) == 'pengguna':
        cursor.execute("SELECT * FROM PENGGUNA WHERE email = %s", [email])
    elif str.lower(table_name) == 'cs':
        cursor.execute("SELECT * FROM CS WHERE email = %s", [email])
    elif str.lower(table_name) == 'konsumen':
        cursor.execute("SELECT * FROM KONSUMEN WHERE email = %s", [email])
    elif str.lower(table_name) == 'admin_apotek':
        cursor.execute("SELECT * FROM ADMIN_APOTEK WHERE email = %s", [email])
    else:
        cursor.execute("SELECT * FROM KURIR WHERE email = %s", [email])
    pengguna = dictfetchall(cursor)
    return pengguna

def get_biggest_id_on_specific_table(table_name):
    cursor = connection.cursor()
    if str.lower(table_name) == 'konsumen':
        cursor.execute("SELECT * FROM KONSUMEN ORDER BY id_konsumen DESC LIMIT 1")
        if konsumen := cursor.fetchone():
            return konsumen[0]
        else:
            return '0'
    elif str.lower(table_name) == 'kurir':
        cursor.execute("SELECT * FROM KURIR ORDER BY id_kurir DESC LIMIT 1")
        if kurir := cursor.fetchone():
            return kurir[0]
        else:
            return '0'
    else:
        return 'not_found'


def insert_pengguna_with_role(table_name, data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO PENGGUNA VALUES (%s, %s, %s, %s)", [data['email'],data['telepon'],data['password'],data['nama']])
    if str.lower(table_name) == 'admin_apotek':
        cursor.execute("INSERT INTO APOTEKER VALUES (%s)", [data['email']])
        cursor.execute("INSERT INTO ADMIN_APOTEK VALUES (%s, NULL)", [data['email']])
        return
    elif str.lower(table_name) == 'cs':
        cursor.execute("INSERT INTO APOTEKER VALUES (%s)", [data['email']])
        cursor.execute("INSERT INTO CS VALUES (%s, %s, %s)", [data['no_ktp'], data['email'], data['no_sia']])
        return
    elif str.lower(table_name) == 'konsumen':
        cursor.execute("INSERT INTO KONSUMEN VALUES (%s, %s, %s, %s)", [data['id_konsumen'], data['email'], data['sex'], data['tanggalLahir']])
        for statusAlamat, namaAlamat in data['alamats']:
            cursor.execute("INSERT INTO ALAMAT_KONSUMEN VALUES (%s, %s, %s)", [data['id_konsumen'], namaAlamat, statusAlamat])
        return
    elif str.lower(table_name) == 'kurir':
        cursor.execute("INSERT INTO KURIR VALUES (%s, %s, %s)", [data['id_kurir'], data['email'], data['perusahaan']])
    else:
        return
