from django.shortcuts import render, HttpResponse, redirect
from .models import *

# Create your views here.

def index(request):
    data = {}
    if request.session.get('user') is not None:
        data['apoteks'] = fetch_all_apotek()
        return render(request,'apotek/index.html', data)
    else:
        return redirect('authentication:index')

def create(request):
    data = {}
    if request.session.get('user'):
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == "GET":
                return render(request,'apotek/create.html')
            else:
                current_biggest_apotek_id = get_biggest_apotek_id()
                current_biggest_apotek_id_number = ''.join(filter(str.isdigit, current_biggest_apotek_id))
                current_biggest_apotek_id_alphabet = current_biggest_apotek_id.strip(current_biggest_apotek_id_number)
                new_apotek_id_number = int(current_biggest_apotek_id_number) + 1

                data['id_apotek'] = current_biggest_apotek_id_alphabet+ str(new_apotek_id_number)
                data['alamat_apotek'] = request.POST['alamatApotek']
                data['telepon_apotek'] = request.POST['nomorTelepon']
                data['email'] = request.POST['emailPenyelenggara']
                data['no_sia'] = request.POST['noSiaPenyelenggara']
                data['nama_apotek'] = request.POST['namaApotek']
                data['nama_penyelenggara'] = request.POST['namaPenyelenggara']
                
                if check_email_sia_alamat_uniqeu(data['email'], data['alamat_apotek'], data['no_sia']):
                    insert_apotek(data)
                    return redirect('apotek:index')
                else:
                    return render(request, 'apotek/create.html', data)
        else:
            return redirect('apotek:index')
    else:
        return redirect('authentication:index')

# TODO: finish edit mthod & edit html
def edit(request, id_apotek):
    data = {}
    if request.session.get('user'):
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == "GET":
                if((apotek:=get_apotek(id_apotek)) is not None):
                    data['apotek'] = apotek
                    return render(request,'apotek/edit.html', data)
                else:
                    return redirect('apotek:index')
            else:
                data['id_apotek'] = id_apotek
                data['nama_apotek'] = request.POST['namaApotek']
                data['alamat_apotek'] = request.POST['alamatApotek']
                data['telepon_apotek'] = request.POST['nomorTelepon']
                data['nama_penyelenggara'] = request.POST['namaPenyelenggara']
                data['no_sia'] = request.POST['noSiaPenyelenggara']
                data['email'] = request.POST['emailPenyelenggara']
                update_apotek(data)
                return redirect('apotek:index')
        else:
            return redirect('apotek:index')
    else:
        return redirect('authentication:index')

def delete(request, id_apotek):
    if request.session.get('user'):
        if request.session['user']['role'] == 'admin_apotek':
            if request.method == "POST":
                delete_apotek(id_apotek)
        return redirect('apotek:index')
    else:
        return redirect('authentication:index')


