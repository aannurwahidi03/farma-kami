from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_apotek():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM apotek")
    return dictfetchall(cursor)

def get_biggest_apotek_id():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM APOTEK ORDER BY id_apotek DESC LIMIT 1")
        if apotek:= cursor.fetchone():
            return apotek[0]
        else:
            return "0"

def check_email_sia_alamat_uniqeu(email, alamat_apotek, sia):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM APOTEK WHERE email = %s", [email])
        if cursor.fetchone():
            return False
        cursor.execute("SELECT * FROM APOTEK WHERE alamat_apotek = %s", [alamat_apotek])
        if cursor.fetchone():
            return False
        cursor.execute("SELECT * FROM APOTEK WHERE no_sia = %s", [sia])
        if cursor.fetchone():
            return False
        return True

def get_apotek(id_apotek):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM APOTEK WHERE id_apotek = %s", [id_apotek])
        if any(apotek:=dictfetchall(cursor)):
            return apotek[0]
        else:
            return None

def insert_apotek(data):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO APOTEK VALUES (%s, %s, %s, %s, %s, %s, %s)",
        [
            data['id_apotek'], data['email'], data['no_sia'], data['nama_penyelenggara'],
            data['nama_apotek'], data['alamat_apotek'], data['telepon_apotek']
        ])
        return

# TODO : create edit method
def update_apotek(data):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE APOTEK SET "+
        "email = %s, no_sia = %s, nama_penyelenggara = %s, nama_apotek = %s, alamat_apotek = %s, telepon_apotek = %s "+
        "WHERE id_apotek = %s", 
        [
            data['email'], data['no_sia'], data['nama_penyelenggara'],
            data['nama_apotek'], data['alamat_apotek'], data['telepon_apotek'], data['id_apotek']
        ])

def delete_apotek(id_apotek):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM APOTEK WHERE id_apotek = %s", [id_apotek])
        return