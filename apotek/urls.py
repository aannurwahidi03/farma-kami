from django.urls import path, include
from . import views

#TO DO : Tambahin URLSnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
urlpatterns = [
    path('', views.index, name='index'),
    path('create/', views.create, name='create'),
    path('<str:id_apotek>/edit/', views.edit, name='edit'),
    path('<str:id_apotek>/delete/', views.delete, name='delete')
]
