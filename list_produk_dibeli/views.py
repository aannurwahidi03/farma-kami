from django.shortcuts import render, HttpResponse, redirect
from .models import *
from transaksi_pembelian.models import fetch_all_transaksi_pembelian
from produk_apotek.models import fetch_all_produk_apotek, get_produk_apotek

# Create your views here.

#TO DO : Benerin returnnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
def index(request):
    data = {}
    if request.session.get('user') is not None:
        data['list_produk_dibeli'] = fetch_all_list_produk_dibeli()
        return render(request,'list_produk_dibeli/index.html', data)
    else:
        return redirect('authentication:index')

def create(request):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek'):
            data['transaksi_pembelian'] = fetch_all_transaksi_pembelian()
            data['produk_apotek'] = fetch_all_produk_apotek()
            if request.method == 'GET':
                return render(request,'list_produk_dibeli/create.html', data)
            else:
                data['idProduk'] = request.POST['idProduk']
                data['idApotek'] = request.POST['idApotek']
                data['idTransaksi'] = request.POST['idTransaksi']
                data['jumlah'] = request.POST['jumlah']
                if (
                    any(get_specific_list_produk_dibeli( data['idApotek'], data['idProduk'], data['idTransaksi'])) 
                    and 
                    any(get_produk_apotek(data['idProduk'], data['idApotek']))
                    ):
                    data['error'] = "kombinasi id_produk dan id_apotek salah!"
                    return render(request, 'list_produk_dibeli/create.html', data)
                else:
                    insert_list_produk_dibeli(data)
                    return redirect('list_produk_dibeli:index')
        else:
            return redirect('list_produk_dibeli:index')
    else:
        return redirect('authentication:index')


def edit(request, id_apotek, id_produk, id_transaksi_pembelian):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek'):
            if request.method == 'GET':
                if any(get_specific_list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian)):
                    data['transaksi_pembelian'] = fetch_all_transaksi_pembelian()
                    data['list_produk_dibeli_item'] = get_specific_list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian)[0]
                    return render(request,'list_produk_dibeli/edit.html', data)
                else:
                    return redirect('list_produk_dibeli:index')
            else:
                data['idProduk'] = id_produk
                data['idApotek'] = id_apotek
                data['idTransaksiCurrent'] = id_transaksi_pembelian
                data['idTransaksiUpdate'] = request.POST['idTransaksi']
                data['jumlah'] = request.POST['jumlah']
                print(data)

                update_list_produk_dibeli(data)
                return redirect('list_produk_dibeli:index')
        else:
            return redirect('list_produk_dibeli:index')
    else:
        return redirect('authentication:index')

def delete(request, id_apotek, id_produk, id_transaksi_pembelian):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek'):
             if request.method == 'POST':
                delete_list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian)
        return redirect('list_produk_dibeli:index')
    else:
        return redirect('authentication:index')
