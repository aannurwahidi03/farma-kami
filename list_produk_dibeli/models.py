from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_list_produk_dibeli():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM list_produk_dibeli")
    return dictfetchall(cursor)

def insert_list_produk_dibeli(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO LIST_PRODUK_DIBELI VALUES (%s, %s, %s, %s)", 
    [
        data['jumlah'], data['idApotek'], data['idProduk'], data['idTransaksi']
    ])
    return

def get_specific_list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM LIST_PRODUK_DIBELI WHERE id_produk = %s AND id_apotek = %s AND id_transaksi_pembelian = %s", 
    [
        id_produk, id_apotek, id_transaksi_pembelian
    ])
    return dictfetchall(cursor)

def update_list_produk_dibeli(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE LIST_PRODUK_DIBELI SET " + 
    "jumlah = %s, id_transaksi_pembelian = %s WHERE id_produk = %s AND id_apotek = %s AND id_transaksi_pembelian = %s",
    [
        data['jumlah'], data['idTransaksiUpdate'], data['idProduk'], data['idApotek'], data['idTransaksiCurrent']
    ])
    return

def delete_list_produk_dibeli(id_apotek, id_produk, id_transaksi_pembelian):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM LIST_PRODUK_DIBELI WHERE id_produk = %s AND id_apotek = %s AND id_transaksi_pembelian = %s", [id_produk, id_apotek, id_transaksi_pembelian])
    return

# def get_id_apotek_from_id_produk(id_produk):
#     cursor = connection.cursor()
#     cursor.execute("SELECT id_apotek FROM PRODUK_APOTEK WHERE id_produk = %s", [id_produk])
#     return