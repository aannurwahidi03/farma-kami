$(document).ready(() => {
    date_init();
    
    $("button#tambahAlamat").click(()=>{
        let inputGroupAlamat = $("#inputGroupAlamat")
        let clonedFormGroup = $('#inputGroupAlamat div.form-group').first().clone()
        clonedFormGroup.find('.form-control').attr('value', '')
        clonedFormGroup.appendTo(inputGroupAlamat)
    });
});

function removeAlamat(element) {
    $(element).parentsUntil("#inputGroupAlamat").remove()
};