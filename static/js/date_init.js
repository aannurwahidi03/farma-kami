function date_init(tanggal_awal='2020-01-01') {
    $('.date-picker').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
            value: tanggal_awal
        });
}