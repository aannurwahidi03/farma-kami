from django.db import models, connection
from converter.fetch_to_dict import dictfetchall
# Create your models here.

def fetch_all_pengantaran_farmasi():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM pengantaran_farmasi")
    return dictfetchall(cursor)

def get_biggest_id_pengantaran():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM PENGANTARAN_FARMASI ORDER BY id_pengantaran DESC LIMIT 1")
    pengantaran_farmasi = cursor.fetchone()
    return pengantaran_farmasi[0]

def insert_pengantaran_farmasi(data):
    cursor = connection.cursor()
    cursor.execute("INSERT INTO PENGANTARAN_FARMASI VALUES (%s, %s, %s, %s, %s, %s, 0)", 
    [
        data['idPengantaran'], data['idKurir'], data['idTransaksi'], data['waktu'], data['statusPengantaran'],
        data['biayaKirim']
    ])
    return

def get_specific_pengantaran_farmasi(id_pengantaran):
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM PENGANTARAN_FARMASI WHERE id_pengantaran = %s", [id_pengantaran])
    return dictfetchall(cursor)

def fetch_all_kurir():
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM KURIR")
    return dictfetchall(cursor)

def update_pengantaran_farmasi(data):
    cursor = connection.cursor()
    cursor.execute("UPDATE PENGANTARAN_FARMASI SET " + 
    "id_kurir = %s, id_transaksi_pembelian = %s, waktu = %s, status_pengantaran = %s, biaya_kirim = %s WHERE id_pengantaran = %s",
    [
        data['idKurir'], data['idTransaksi'], data['waktu'], data['status'], data['biayaKirim'], 
        data['idPengantaran']
    ])
    return

def delete_pengantaran_farmasi(id_pengantaran):
    cursor = connection.cursor()
    cursor.execute("DELETE FROM PENGANTARAN_FARMASI WHERE id_pengantaran = %s", [id_pengantaran])
    return