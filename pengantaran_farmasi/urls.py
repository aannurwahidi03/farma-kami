from django.urls import path, include
from . import views

#TO DO : Tambahin URLSnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
urlpatterns = [
    path('', views.index, name='index'),
    path('create', views.create, name='create'),
    path('<str:id_pengantaran>/edit/', views.edit, name='edit'),
    path('<str:id_pengantaran>/delete/', views.delete, name='delete')
]
