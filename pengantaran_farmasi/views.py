from django.shortcuts import render, HttpResponse, redirect
from .models import *
from transaksi_pembelian.models import fetch_all_transaksi_pembelian
import datetime
from random import *


# Create your views here.

#TO DO : Benerin returnnya, ikutin aja yang di app AUTHENTICATION / TRANSAKSI RESEP / ALAT MEDIS
def index(request):
    data = {}
    if request.session.get('user') is not None:
        data['pengantaran_farmasi'] = fetch_all_pengantaran_farmasi()
        return render(request,'pengantaran_farmasi/index.html', data)
    else:
        return redirect('authentication:index')

def create(request):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek'):
            if request.method == 'GET':
                data['pengantaran_farmasi'] = fetch_all_pengantaran_farmasi()
                return render(request,'pengantaran_farmasi/create.html', data)
            else:
                if(any(kurirs := fetch_all_kurir())):
                    kurir = kurirs[randrange(len(kurirs)-1)]

                current_biggest_id_pengantaran = get_biggest_id_pengantaran()
                current_biggest_id_pengantaran_number = ''.join(filter(str.isdigit, current_biggest_id_pengantaran))
                current_biggest_id_pengantaran_alphabet = current_biggest_id_pengantaran.strip(current_biggest_id_pengantaran_number)
                new_pengantaran_farmasi_id_pengantaran = int(current_biggest_id_pengantaran_number) + 1

                data['idPengantaran'] = current_biggest_id_pengantaran_alphabet + str(new_pengantaran_farmasi_id_pengantaran)
                data['idTransaksi'] = request.POST['idTransaksi']
                data['waktu'] = str(datetime.datetime.now())
                data['biayaKirim'] = request.POST['biayaKirim']
                data['idKurir'] = kurir['id_kurir']
                data['statusPengantaran'] = 'Not Delivered'

                insert_pengantaran_farmasi(data)
                return redirect('pengantaran_farmasi:index')
        else:
            return redirect('pengantaran_farmasi:index')
        
    else:
        return redirect('authentication:index')

def edit(request, id_pengantaran):
    data = {}
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek'):
            if request.method == 'GET':
                if any(pengantaran:=get_specific_pengantaran_farmasi(id_pengantaran)):
                    data['transaksi_pembelian'] = fetch_all_transaksi_pembelian()
                    data['kurir'] = fetch_all_kurir()
                    data['waktuSekarang'] = pengantaran[0]['waktu']
                    data['pengantaran_farmasi'] = fetch_all_pengantaran_farmasi()
                    data['pengantaran_farmasi_item'] = get_specific_pengantaran_farmasi(id_pengantaran)[0]
                    return render(request,'pengantaran_farmasi/edit.html', data)
                else:
                    return redirect('pengantaran_farmasi:index')
            else:
                data['idPengantaran'] = id_pengantaran
                data['idKurir'] = request.POST['idKurir']
                data['idTransaksi'] = request.POST['idTransaksi']
                data['waktu'] = str(datetime.datetime.now())
                data['status'] = request.POST['status']
                data['biayaKirim'] = request.POST['biayaKirim']

                update_pengantaran_farmasi(data)
                return redirect('pengantaran_farmasi:index')
        else:
            return redirect('pengantaran_farmasi:index')
    else:
        return redirect('authentication:index')

def delete(request, id_pengantaran):
    if request.session.get('user') is not None:
        if (request.session['user']['role'] == 'admin_apotek'):
            if request.method == 'POST':
                delete_pengantaran_farmasi(id_pengantaran)
        return redirect('pengantaran_farmasi:index')
    else:
        return redirect('authentication:index')
